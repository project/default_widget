<?php

namespace Drupal\default_widget\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\field_ui\Form\FieldStorageAddForm;

/**
 * Provides a Default Widget form.
 */
class DefaultWidgetFieldStorageAddForm extends FieldStorageAddForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_widget_field_storage_add';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('default_widget.settings');
    $field_type = $form_state->getValue('new_storage_type') ?? $form_state->getValue('existing_storage_name');

    // Only do anything extra if we have a default widget configured for this
    // field.
    $default_widget = $config->get("$field_type.default_widget");
    if (!$default_widget) {
      return;
    }

    // Update the default widget for the field.
    $field_name = $form_state->getValue('field_name');
    $this->configureEntityFormDisplay($field_name, $default_widget, []);
  }

}
