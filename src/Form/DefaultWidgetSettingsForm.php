<?php

namespace Drupal\default_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure default_widget settings for this site.
 */
class DefaultWidgetSettingsForm extends ConfigFormBase {

  /**
   * An array with all field type UI definitions, keyed by field type.
   *
   * @var array
   */
  protected $fieldTypeUiDefinitions;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManager
   */
  protected $fieldTypePluginManager;

  /**
   * The widget plugin manager.
   *
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  protected $widgetPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->widgetPluginManager = $container->get('plugin.manager.field.widget');
    $instance->fieldTypePluginManager = $container->get('plugin.manager.field.field_type');
    $instance->fieldTypeUiDefinitions = $instance->fieldTypePluginManager->getUiDefinitions();
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_widget_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['default_widget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('default_widget.settings');

    foreach ($this->fieldTypeUiDefinitions as $field_type => $definition) {

      // Sanitize preconfigured field type names. Most functions only accept the
      // base field type name, not the fully qualified preconfigured field type
      // name.
      $type = $field_type;
      $preconfigured_field_type = explode(':', $field_type);
      if (count($preconfigured_field_type) > 1) {
        $type = $preconfigured_field_type[1];
      }

      // Create the category grouping on the form if it doesn't exist already.
      $category = (string) $definition['category'];
      if (!isset($form[$category])) {
        $form[$category] = [
          '#type' => 'fieldset',
          '#title' => $definition['category'],
        ];
      }

      // Add the field type to the form.
      $form[$category][$field_type] = [
        '#type' => 'select',
        '#title' => $definition['label'],
        '#options' => $this->widgetPluginManager->getOptions($type),
        '#default_value' => $config->get("$field_type.default_widget") ?? $definition['default_widget'],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Remove any config values that already match the field type default
    // widget, to avoid useless configuration items.
    foreach ($this->fieldTypeUiDefinitions as $field_type => $definition) {
      if ($form_state->getValue($field_type) == $definition['default_widget']) {
        $form_state->unsetValue($field_type);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('default_widget.settings');

    // Set the new default_widget configuration values.
    $field_types_to_add = $form_state->cleanValues()->getValues();
    foreach ($field_types_to_add as $field_type => $default_widget) {
      $config->set("$field_type.default_widget", $default_widget);
    }
    // Remove any configuration that already matches the field type sane
    // defaults, to avoid useless configuration items. Then save the config.
    $field_types_to_remove = array_diff_key($this->fieldTypeUiDefinitions, $field_types_to_add);
    foreach ($field_types_to_remove as $field_type => $definition) {
      $config->clear("$field_type");
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
