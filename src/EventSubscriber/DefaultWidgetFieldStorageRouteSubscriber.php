<?php

namespace Drupal\default_widget\EventSubscriber;

use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Default Widget route subscriber.
 *
 * Extends all the FieldStorageAddForm routes, so we can add extra processing
 * after submit.
 */
class DefaultWidgetFieldStorageRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($collection->all() as $route_name => $route) {
      // Extend the FieldStorageAddForm so we can add our own widget defaults
      // after field creation through the UI.
      if (strpos($route_name, 'field_ui.field_storage_config_add_') === 0) {
        $route->setDefault('_form', '\Drupal\default_widget\Form\DefaultWidgetFieldStorageAddForm');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();

    // Use a lower priority than \Drupal\views\EventSubscriber\RouteSubscriber
    // to ensure the requirement will be added to its routes.
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -300];

    return $events;
  }

}
