INTRODUCTION
------------

This module allows you to configure the default widget for any UI Field Type
(Manage Form Display).

Sometimes, the sane defaults are not really ideal for a project (I.e. we want
all Entity Reference field for taxonomies to be a Select list by default), and
we have to keep re configuring it the same way every time we add a new field to
a content type. This module aims to minimize that pain.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/default_widget

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/default_widget

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Configure the user permissions in Administration » People » Permissions:

   - Administer Default Widget configuration

     Allows a user to change the default widgets for any UI field type on this
     module's configuration form.

 * Customize the default widget settings in Administration » Configuration  »
   User interface » Default Widget Settings.

MAINTAINERS
-----------

Current maintainers:
 * Miguel Guerreiro (gueguerreiro) - https://www.drupal.org/user/3589301
